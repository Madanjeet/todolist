var count = 0;
var centralDataStructure = [];
var centralStorage = window.localStorage;

var progress = document.getElementById('bar');

if (window.localStorage.todoList) {
    centralDataStructure = JSON.parse(localStorage.getItem("todoList"));
    count = centralDataStructure.length;
}
var submission = document.getElementById('submit');

submission.onclick = pressSubmit;

function submitbyEnterKey(event) {
    var enterKey = event.key | event.which;

    if (enterKey === 13) {
        pressSubmit();
    }
}

function pressSubmit() {
    var newTodo = document.getElementById('todo-input').value;
    if (newTodo === "") {
        alert("please provide some valid input");
    } else {
        document.getElementById('todo-input').value = "";
        var obj = {
            id: ++count,
            text: newTodo,
            chk: false,
            del: false
        }
        centralDataStructure.push(obj);
        centralStorage.setItem("todoList", JSON.stringify(centralDataStructure));
        //console.log(centralDataStructure);//to be removed.....
        refreshData();

    }
}
refreshData();

function refreshData() {
    document.getElementById('ordered-list').innerHTML = "";
    var centralDataStructure = JSON.parse(centralStorage.getItem("todoList"));
    if (centralDataStructure)
        progress.value = 0;
    for (i = 0; i < centralDataStructure.length; i++) {
        if (centralDataStructure[i].del === false) {

            progress.max = centralDataStructure.length;

            // console.log("Inside loop");
            var orderedList = document.getElementById("ordered-list");
            orderedList.setAttribute("ondrop", "dropEvent(event)");

            var item = document.createElement('li');
            item.id = centralDataStructure[i].id;

            var spanText = document.createElement('span');
            var textLi = document.createTextNode(centralDataStructure[i].text);
            spanText.appendChild(textLi);

            var checkboxGenerate = getCheckBox();
            if (centralDataStructure[i].chk === true) {
                ++progress.value;
                checkboxGenerate.checked = true;
                spanText.className = "alreadyChecked";
            } else {
                checkboxGenerate.checked = false;
                spanText.className = "unchecked";
            }
            var cancelSpan = getCancelSpan();
            cancelSpan.className = "cancel";

            item.appendChild(checkboxGenerate);
            item.appendChild(spanText);
            item.appendChild(cancelSpan);

            orderedList.appendChild(item);
            item.setAttribute('draggable', true);
            item.setAttribute('ondragstart', 'dragStart(event)');
            item.setAttribute('ondragover', 'dragOver(event)');
        }
    }
}

function getCheckBox() {
    var checkBox = document.createElement('input');
    checkBox.type = "checkbox";
    checkBox.setAttribute('onClick', 'checkedEvent(this)');
    return checkBox;

}

function checkedEvent(e) {
    for (i = 0; i < centralDataStructure.length; i++) {
        if (centralDataStructure[i].id == e.parentNode.id) {
            if (e.checked) {
                centralDataStructure[i].chk = true;
            } else {
                centralDataStructure[i].chk = false;
            }
        }
    }
    centralStorage.setItem('todoList', JSON.stringify(centralDataStructure));
    refreshData();
}

function getCancelSpan() {
    var span = document.createElement('span');
    var cancelIcon = document.createTextNode('\u00d7');
    span.appendChild(cancelIcon);
    span.setAttribute('onClick', 'deleteTodo(this)');
    return span;
}

function deleteTodo(e) {
    for (i = 0; i < centralDataStructure.length; i++) {
        if (centralDataStructure[i].id == e.parentNode.id) {
            centralDataStructure[i].del = true;
            centralDataStructure.splice(i, 1);
        }
    }
    centralStorage.setItem('todoList', JSON.stringify(centralDataStructure));
    refreshData();
}

function dragStart(event) {
    dragStartId = event.target.id;
}

function dragOver(event) {
    event.preventDefault();
    dragOverId = event.target.id;
}

function dropEvent(event) {
    // console.log(dragStartId);
    // console.log(dragOverId);
    var startIndex = 0;
    var endIndex = 0;
    var dragStartElement = document.getElementById(dragStartId);
    var dragOverElement = document.getElementById(dragOverId);
    for (i = 0; i < centralDataStructure.length; i++) {
        if (dragStartId == centralDataStructure[i].id) {
            startIndex = i;
        }
        if (dragOverId == centralDataStructure[i].id) {
            endIndex = i;
        }
    }
    // console.log("Start Indedx"+startIndex);
    // console.log(endIndex);
    var temp = centralDataStructure[startIndex];
    centralDataStructure.splice(startIndex, 1);
    centralDataStructure.splice(endIndex, 0, temp);
    centralStorage.setItem('todoList', JSON.stringify(centralDataStructure));
    refreshData();

}